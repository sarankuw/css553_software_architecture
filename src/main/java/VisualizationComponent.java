import c2.framework.*;

import java.time.Clock;
import java.util.ArrayList;
import java.util.HashSet;


/**
 * Created by saranyakrishnan on 5/24/17.
 */
public class VisualizationComponent {

    public static void main(String[] args) throws Exception {

        Architecture visualizationComponent = new SimpleArchitecture("visualization component");


        //Create the components
       // Component githubRepository = new GithubRepositoryComponent();
        Component projectDocumentation = new ProjectDocumentationComponent();
        Component documentParser =  new DocumentationParser();
        Component sourceCodeParser =  new SourceCodeParser();
       // Component githubParser =  new GithubParserComponent();
        Component architectureGUI =  new ArchitectureGUIComponent();


        //Create the connectors
        Connector connector1 = new ConnectorThread("Connector1");
        Connector connector2 = new ConnectorThread("Connector2");
        Connector connector3 = new ConnectorThread("Connector3");



        //Add the components and connectors to the architecture

        //visualizationComponent.addComponent(githubRepository);
        visualizationComponent.addComponent(projectDocumentation);
        visualizationComponent.addComponent(documentParser);
        visualizationComponent.addComponent(sourceCodeParser);
        //visualizationComponent.addComponent(githubParser);
        visualizationComponent.addComponent(architectureGUI);

        visualizationComponent.addConnector(connector1);
        visualizationComponent.addConnector(connector2);
        visualizationComponent.addConnector(connector3);


        //Create the welds (links) between components and connectors
        //visualizationComponent.weld(githubRepository,connector1);
        visualizationComponent.weld(projectDocumentation,connector1);
        visualizationComponent.weld(connector1,documentParser);
       // visualizationComponent.weld(connector1,sourceCodeParser);
        //visualizationComponent.weld(connector1,githubParser);

        visualizationComponent.weld(documentParser,connector2);
        visualizationComponent.weld(connector2,sourceCodeParser);

        //visualizationComponent.weld(githubParser,connector3);
        visualizationComponent.weld(sourceCodeParser,connector3);
        visualizationComponent.weld(connector3,architectureGUI);

        visualizationComponent.start();

        //System.out.println("Total time = [" + (endTime - startTime) + "]");




    }


}
