import c2.framework.ComponentThread;
import c2.framework.FIFOPort;
import c2.framework.Notification;
import c2.framework.Request;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

public class DocumentationParser extends ComponentThread {


    //Documentation parser
    String textFile = "files/Document.txt";
    String stopWords = "files/Stopwords.txt";
    String outputFile = "files/Output.txt";
    String keywords = "files/Keywords.txt";
    ArrayList<String> keywordsList = new ArrayList<String>();


    public ArrayList<String> getKeywordsArrayList() {
        return keywordsArrayList;
    }

    public void setKeywordsArrayList(ArrayList<String> keywordsArrayList) {
        this.keywordsArrayList = keywordsArrayList;
    }

    private ArrayList<String> keywordsArrayList;


    public DocumentationParser() {
        super.create("documentationparser", FIFOPort.class);
    }


    public void start() {
        super.start();
        try {
            Request r = new Request("requestDocuments");
            send(r);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public HashSet readStopWords(String stopWords) throws Exception {
        HashSet<String> set = new HashSet<String>();
        try {
            Scanner stopWordsFile = new Scanner(new File(stopWords));
            String word;
            while (stopWordsFile.hasNext()) {
                word = stopWordsFile.next();
                set.add(word);
            }
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
        return set;
    }

    public void getKeywords(String textFile, HashSet<String> set, String outputFile) throws Exception {
        try {
            Scanner file = new Scanner(new File(textFile));
            String word;
            PrintWriter writer = new PrintWriter(new File(outputFile));
            while (file.hasNext()) {
                word = file.next();
                if (!set.contains(word)) {
                    writer.print(word + " ");
                }
            }
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public ArrayList<String> getList(String keywords, ArrayList<String> keywordsList) throws Exception {
        try {
            Scanner file = new Scanner(new File(keywords));
            String word;
            while (file.hasNext()) {
                word = file.next();
                keywordsList.add(word);
            }
            this.keywordsArrayList = keywordsList;
            return keywordsList;
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    protected void handle(Request r) {
        if (r.name().equals("getKeywords")) {
            Notification n = createStateNotification();
            send(n);
        }
    }


    protected void handle(Notification n) {
        if(n.name().equals("returnDocuments")){
            HashSet<String> set = null;
            try {
                set = readStopWords(stopWords);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                getKeywords(textFile, set, outputFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                getList(keywords, keywordsList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    private Notification createStateNotification() {
        Notification n = new Notification("returnKeywords");
        n.addParameter("keywords", getKeywordsArrayList());
        return n;
    }
}
