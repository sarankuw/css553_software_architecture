import c2.framework.ComponentThread;
import c2.framework.FIFOPort;
import c2.framework.Notification;
import c2.framework.Request;

import java.io.IOException;

/**
 * Created by saranyakrishnan on 6/1/17.
 */
public class ArchitectureGUIComponent extends ComponentThread {

    public ArchitectureGUIComponent() {
        super.create("archtectureGUIComponent", FIFOPort.class);
    }


    public void start() {
        super.start();
        try {
            Request r = new Request("getJson");
            send(r);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    protected void handle(Request r) {

    }

    protected void handle(Notification n) {
        if (n.name().equals("returnJson")) {
            //open safari in
            Runtime rt = Runtime.getRuntime();
            String url = "/Users/saranyakrishnan/ArchitectureTree/index.html";
            try {
                rt.exec("open " + url);
                long endTime = System.currentTimeMillis();
                System.out.println("End time = [" + endTime + "]");

            } catch (IOException e) {
                e.printStackTrace();
            }

        }


    }
}
