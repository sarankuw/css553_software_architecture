import java.util.ArrayList;

/**
 * Created by saranyakrishnan on 5/13/17.
 */
public class Package {

    private String name;
    private ArrayList<JavaClass> children;
    private String url;
    private ArrayList<String> dependsOn;
    private ArrayList<String> technos;
    private String satisfaction;
    private String host;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<String> getDependsOn() {
        return dependsOn;
    }

    public void setDependsOn(ArrayList<String> dependsOn) {
        this.dependsOn = dependsOn;
    }

    public void setTechnos(ArrayList<String> technos) {
        this.technos = technos;
    }

    public String getSatisfaction() {
        return satisfaction;
    }

    public void setSatisfaction(String satisfaction) {
        this.satisfaction = satisfaction;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<JavaClass> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<JavaClass> children) {
        this.children = children;
    }








}
