import c2.framework.ComponentThread;
import c2.framework.FIFOPort;
import c2.framework.Notification;
import c2.framework.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by saranyakrishnan on 6/1/17.
 */
public class ProjectDocumentationComponent extends ComponentThread {

    String url = "http://s3.thinkaurelius.com/docs/titan/1.0.0/index-admin.html";
    String textFile = "files/Document.txt";

    public ProjectDocumentationComponent() {
        super.create("projDocComponent", FIFOPort.class);
    }

    public void start() {
        super.start();
        long startTime = System.currentTimeMillis();
        System.out.println("start time = [" + startTime + "]");

        try {
            getTextFile(url,textFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void handle(Request r) {
        if (r.name().equals("requestDocuments")) {
            Notification n = createStateNotification();
            send(n);
        }

    }

    public void getTextFile(String url, String textFile) throws Exception {
        Document doc = Jsoup.connect(url).get();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(textFile));
            writer.write(doc.text());
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    protected void handle(Notification n) {
    }

    private Notification createStateNotification() {
        Notification n = new Notification("returnDocuments");
        return n;
    }
}
