import java.lang.*;
import java.util.ArrayList;

/**
 * Created by saranyakrishnan on 5/13/17.
 */
public class Keywords {

    private String name;
    private ArrayList <Package> children;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Package> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<Package> children) {
        this.children = children;
    }


}
