import c2.framework.ComponentThread;
import c2.framework.FIFOPort;
import c2.framework.Notification;
import c2.framework.Request;

/**
 * Created by saranyakrishnan on 6/1/17.
 */
public class GithubRepositoryComponent extends ComponentThread {
    public GithubRepositoryComponent() {
        super.create("githubRepoComponent", FIFOPort.class);
    }

    protected void handle(Request r) {

    }

    protected void handle(Notification n) {

    }
}
