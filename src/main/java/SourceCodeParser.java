/**
 * Created by saranyakrishnan on 5/12/17.
 */

import c2.framework.ComponentThread;
import c2.framework.FIFOPort;
import c2.framework.Notification;
import c2.framework.Request;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;


public class SourceCodeParser extends ComponentThread {

    ArrayList<Package> packageList = new ArrayList<Package>();
    ArrayList<String> listFromDocument = new ArrayList<String>();
    String inputFilePath = "files/xml_input.xml";


    public SourceCodeParser() {
        super.create("sourcecodeparser", FIFOPort.class);
    }

    public void start() {
        super.start();
        try {
            xmlToJsonConverter(inputFilePath);
            formatJson("files/rawdata.json");

            Thread sourceCodeParserThread = new Thread(){
                public void run(){
                    //Repeat while the application runs
                    while(true){
                        //Wait for five seconds
                        try{
                            Thread.sleep(1000);
                        }
                        catch(InterruptedException ie){}
                        Request r = new Request("getKeywords");
                        send(r);
                    }
                }
            };
            sourceCodeParserThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //XML to JSON Converter - This method converts XML to JSON
    public void xmlToJsonConverter(String xmlFilePath) throws FileNotFoundException {

        //Read the xml file
        BufferedReader br = new BufferedReader(new FileReader(new File(xmlFilePath)));
        String line;
        StringBuilder sb = new StringBuilder();

        try {
            while ((line = br.readLine()) != null) {
                sb.append(line.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //convert the xml string to json object
        org.json.JSONObject jsonObject = org.json.XML.toJSONObject(sb.toString());

        //Write json object into file
        try {
            PrintWriter writer = new PrintWriter("files/rawdata.json", "UTF-8");
            writer.print(jsonObject.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void formatJson(String JsonFilePath) {

        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(new FileReader(JsonFilePath));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //Get the JSON object and add it to JSON array
        JSONArray contents = new JSONArray();
        contents.add(obj);
        //Retrieve the first index
        HashMap<String, Object> map = (HashMap<String, Object>) contents.get(0);
        HashMap<String, Object> classDiagramMap = (HashMap<String, Object>) map.get("ClassDiagram");
        HashMap<String, Object> classDiagramComponentsMap = (HashMap<String, Object>) classDiagramMap.get("ClassDiagramComponents");

        JSONArray classDiagramContents = new JSONArray();
        JSONArray enumContents = (JSONArray) classDiagramComponentsMap.get("Enum");
        JSONArray classContents = (JSONArray) classDiagramComponentsMap.get("Class");
        JSONArray interfaceContents = (JSONArray) classDiagramComponentsMap.get("Interface");

        //Add all the contents to classDiagram Contents
        for (int i = 0; i < enumContents.size(); i++) {
            classDiagramContents.add(enumContents.get(i));
        }

        for (int i = 0; i < classContents.size(); i++) {
            classDiagramContents.add(classContents.get(i));
        }
        for (int i = 0; i < interfaceContents.size(); i++) {
            classDiagramContents.add(interfaceContents.get(i));
        }

        HashMap<String, ArrayList<String>> packageMap = new HashMap<String, ArrayList<String>>();
        //Add all the packages and classes/interface/enum into hashmap with package name as "key" and class/interface/enum name as value
        for (int i = 0; i < classDiagramContents.size(); i++) {
            HashMap<String, String> tempMap = (HashMap<String, String>) classDiagramContents.get(i);
            String key = tempMap.get("package");
            String value = tempMap.get("name");
            if (packageMap.containsKey(key)) {
                packageMap.get(key).add(value);
            } else {
                ArrayList<String> tempList = new ArrayList<String>();
                tempList.add(value);
                packageMap.put(key, tempList);
            }

        }

        try {
            buildJson(packageMap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void buildJson(HashMap<String, ArrayList<String>> inputMap) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        Iterator it = inputMap.entrySet().iterator();
        PrintWriter writer = new PrintWriter("files/formatteddata.json", "UTF-8");
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            //System.out.println(pair.getKey() + " = " + pair.getValue());
            ArrayList<String> values = (ArrayList<String>) pair.getValue();
            ArrayList<JavaClass> childList = new ArrayList<JavaClass>();
            for (int i = 0; i < values.size(); i++) {
                JavaClass classObj = new JavaClass();
                classObj.setName(values.get(i));
                childList.add(classObj);
            }
            Package pkgObj = new Package();
            pkgObj.setName((String) pair.getKey());
            pkgObj.setChildren(childList);
            pkgObj.setHost("");

            ArrayList<String> dependsOnList = new ArrayList<String>();
            for (int i = 0; i < childList.size(); i++) {
                dependsOnList.add(childList.get(i).getName());
            }
            pkgObj.setDependsOn(dependsOnList);
            pkgObj.setTechnos(new ArrayList<String>());
            pkgObj.setSatisfaction("");

            //Add the Package object to array list
            packageList.add(pkgObj);
            //Write json object into file
            Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
            writer.print(gson.toJson(pkgObj));
            it.remove(); // avoids a ConcurrentModificationException
        }
        writer.close();
    }

    public void aggregatePackages(ArrayList<String> keywords) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter("files/Data.json", "UTF-8");
        StringBuilder strBuild = new StringBuilder();
        strBuild.append("{\"name\": \"Root\", \"children\": [");
        for (int i = 0; i < keywords.size(); i++) {
            ArrayList<Package> pList = new ArrayList<Package>();
            for (int j = 0; j < packageList.size(); j++) {
                String pkgName = packageList.get(j).getName().toLowerCase();
                String[] pkgNameArray = pkgName.split(Pattern.quote("."));
                String keywordsStr = keywords.get(i).toLowerCase();
                for (int k = 3; k < pkgNameArray.length; k++) {
                    if (keywords.get(i).indexOf(pkgNameArray[k]) != -1) {
                        if (!(pList.contains(packageList.get(j)))) {
                            ArrayList<String> techList = new ArrayList<String>();
                            techList.add(keywords.get(i).toLowerCase());
                            packageList.get(j).setTechnos(techList);
                            pList.add(packageList.get(j));
                        }
                    }
                }
            }
            if (!pList.isEmpty()) {
                Keywords keyWordObj = new Keywords();
                keyWordObj.setName(keywords.get(i).toLowerCase());
                keyWordObj.setChildren(pList);
                //Write json object into file
                Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
                strBuild.append(gson.toJson(keyWordObj));
                strBuild.append(',');

            }

        }
        writer.println(strBuild.substring(0, strBuild.length() - 1));
        writer.print("]}");
        writer.close();
    }


    private ArrayList<String> removeStopWords() {

        ArrayList<String> stopWords = new ArrayList<String>();
        stopWords.add("databases");
        stopWords.add("battled");
        stopWords.add("gio");
        stopWords.add("getrelationindex");
        stopWords.add("getrelationtype");
        stopWords.add("documentation");
        stopWords.add("getgraphindex");
        stopWords.add("graphofthegodsfactory");
        stopWords.add("io");
        stopWords.add("schemaaction");
        stopWords.add("schemastatus");
        stopWords.add("section");
        stopWords.add("illegalargumentexception");
        stopWords.add("battlesbytime");
        stopWords.add("awaitgraphindexstatus");
        stopWords.add("mapreduceindexjobs");
        stopWords.add("mapreduceindexmanagement");
        stopWords.add("mixedexample");
        stopWords.add("deletion");
        stopWords.add("jobs");
        stopWords.add("addvertex");
        stopWords.add("graph");
        stopWords.add("titanmanagement");
        stopWords.add("openmanagement");
        stopWords.add("managementsystem");
        return stopWords;

    }

    protected void handle(Request r) {
        if (r.name().equals("getJson")) {
            Notification n = createStateNotification();
            send(n);
        }

    }

    private Notification createStateNotification() {
        Notification n = new Notification("returnJson");
        String srcFilePath = "files/Data.json";
        String destFilePath = "/Users/saranyakrishnan/ArchitectureTree/";
        File file = new File(srcFilePath);
        File destinationDir = new File(destFilePath);
        try {
            FileUtils.copyFileToDirectory(file, destinationDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("File successfully copied");
        return n;

    }

    protected void handle(Notification n) {
        if (n.name().equals("returnKeywords")) {
            listFromDocument = (ArrayList<String>) n.getParameter("keywords");
            try {
                processResultsfromNotification();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

    }

    void processResultsfromNotification() throws FileNotFoundException, UnsupportedEncodingException {
        ArrayList<String> deleteWordsList = removeStopWords();
        ArrayList<String> finalKeywords = new ArrayList<String>();
        for (int i = 0; i < listFromDocument.size(); i++) {
            if (!(finalKeywords.contains(listFromDocument.get(i))) && !(deleteWordsList.contains(listFromDocument.get(i)))) {
                finalKeywords.add(listFromDocument.get(i));
            }
        }
        //Aggregate Packages based on keywords
        aggregatePackages(finalKeywords);
    }
}
