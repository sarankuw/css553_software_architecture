# **Visualization Tool**

* SourceCodeParser.java has the main method. All other classes are just POJO's for serialization/deserialization.
* The SourceCodeParser.java reads the input XML file and converts it into a json file. This raw json file will be read and converted into a formatted json based on the json format provided by architecture tree.
* Based on the keywords, it aggregates all packages. Keywords will be part of documentation component. Right now it is hardcoded in main program.
* All file paths are harcoded in code now. This needs to be revised.
